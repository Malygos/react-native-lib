# React Native 0.62.2

```
React Native版本：0.62.2
React推荐版本：>= 16.11.0
iOS版本：>= 9.0
```

由于不支持use_framework，所以只能用脚本打包成.a，但是头文件比较难处理
当前React.podspec文件为源码引入方式

此版本以后最低支持iOS 10.0