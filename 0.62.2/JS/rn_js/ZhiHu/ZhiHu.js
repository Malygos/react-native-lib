import React, { useState, useEffect } from "react";
import { 
  AppRegistry, 
  ActivityIndicator,
  FlatList, 
  SafeAreaView, 
  StatusBar, 
  StyleSheet, 
  TouchableOpacity, 
  View,
  Text, 
  Image,
  ImageBackground,
  Button,
  RefreshControl,
} from "react-native";

// 知乎专栏
const ZHColumnModule = () => {
  const [selectedId, setSelectedId] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [hasHeader, setHasHeader] = useState(false);

  const [userData, setUserData] = useState({});
  const [articleListData, setArticleListData] = useState([]);

  const [isRefreshing, setRefreshing] = useState(false);
  const [isLoadingList, setLoadingList] = useState(false);
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    fetch('https://zhuanlan.zhihu.com/api/columns/zhihuadmin')
    .then((response) => response.json())
    .then((json) => {
      setUserData(json)
    })
    .catch((error) => console.error(error))
    .finally(() => {
      setHasHeader(true);
      setLoading(false);
      _onLoadListData(false);
    });
  }, []);

  const FollowButton = () => {
    return(
      <TouchableOpacity style={styles.followButtonContainer} onPress={()=>{}} activeOpacity={1}>
        <Text style={styles.followButtonText}>+关注</Text>
      </TouchableOpacity>
    );
  };

  const ListHeader = ({ userData }) => {
    return(
      <ImageBackground style={ styles.header } source={{uri: userData.author.avatar_url}} blurRadius={8}>
      <View style={{backgroundColor: 'rgba(255, 255, 255, 0.1)', paddingBottom: 10, paddingTop: 44, paddingHorizontal: 10,}}>
        <View style={styles.headerUserInfo}>
          <Image style={styles.headerAvatar} source={{uri: userData.author.avatar_url}}></Image>
          <Text style={styles.headerName} numberOfLines={1}>{userData.author.name}</Text>
          <FollowButton/>
        </View>
        <Text style={{fontSize: 18, color: "white"}} numberOfLines={1} ellipsizeMode={"tail"}>{userData.title}</Text>
        <Text style={{fontSize: 15, color: "white", marginVertical: 8}} numberOfLines={1} ellipsizeMode={"tail"}>{userData.description}</Text>
        <Text style={{fontSize: 12, color: "white"}} numberOfLines={1} ellipsizeMode={"tail"}>{userData.articles_count} 篇内容 · {userData.voteup_count} 赞同</Text>
        </View>
      </ImageBackground>
    );
  };

  const ListItem = ({ item }) => {
    return(
      <TouchableOpacity style={ styles.item } onPress={() => setSelectedId(item.id)} activeOpacity={1}>
        <View style={styles.itemContainer}>
          <Text style={styles.itemTitle}>{item.title}</Text>
          <View style={styles.itemContent}>
            <Text
              style={styles.itemContentMsg} 
              numberOfLines={3}
              ellipsizeMode={"tail"}> 
              {item.excerpt}
            </Text>
            <Image style={styles.itemContentImg} source={{uri: item.title_image}}></Image>
          </View>
          <Text style={styles.itemFootInfo}>文章 · {item.voteup_count}赞同 · {item.comment_count}评论 · {item.created}天前</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const ListFooter = () => {
    return(
      <View>
        <View style={ styles.footer }>
          <ActivityIndicator size='small' animating={true}/>
          <Text style={{ color: '#333' }}>加载更多...</Text>
        </View>
      </View>
    );
  };

  const _onLoadListData = (isLoadMore) => {
    if (isLoadingList) {
      return;
    }
    if (isLoadMore && articleListData.length >= totalCount) {
      return;
    }

    let newPage = 0;

    if (isLoadMore) {
      setLoadingList(true);
      newPage = page + 1;
    }
    
    fetch('https://zhuanlan.zhihu.com/api/columns/zhihuadmin/items?limit=' + pageSize + '&offset=' + newPage*pageSize)
    .then((response) => response.json())
    .then((json) => {
      if (isLoadMore) {
        setArticleListData(articleListData.concat(json.data));
      } else {
        setArticleListData(json.data);
      }
      const totals = json.paging.totals;
      setTotalCount(totals);
    })
    .catch((error) => console.error(error))
    .finally(() => {
      setPage(newPage);
      setLoadingList(false);
    });
  };

  return(
    <SafeAreaView style={styles.container}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList 
          data={ articleListData }
          renderItem={ ListItem }
          keyExtractor={ (item) => item.id }
          extraData={ selectedId }
          ListHeaderComponent={ hasHeader ? <ListHeader userData={userData}/> : null }
          ListFooterComponent={ ListFooter }
          refreshControl={
            <RefreshControl
              refreshing={ isRefreshing }
              colors={ ['#ff0000', '#00ff00', '#0000ff'] }
              tintColor={ '#333' }
              progressBackgroundColor={ '#333' }
              onRefresh={ () => _onLoadListData(false) } />
          }
          onEndReached={ () => _onLoadListData(true) } />
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  header: {
    height: 160,
    backgroundColor: "#8447ff",
    flex: 1,
    flexDirection: "column",
  },
  headerUserInfo: {
    height: 30, 
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  headerAvatar: {
    width: 20, 
    height: 20,
    borderRadius: 15,
  },
  headerName: {
    fontSize: 16,
    color: "white",
    marginHorizontal: 5,
  },
  item: {
    backgroundColor: "#f4f4f4",
    paddingBottom: 10,
  },
  itemContainer: {
    backgroundColor: "white",
    padding: 15,
  },
  itemTitle: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#333333",
  },
  itemContent: {
    flex: 1,
    flexDirection: "row",
    marginVertical: 10,
    padding: 0,
    height: 65,
  },
  itemContentMsg: {
    flex: 1,
    marginRight: 10,
    fontSize: 16,
    color: "#333333",
    lineHeight: 21.5,
  },
  itemContentImg: {
    width: 90,
    resizeMode: "cover",
    borderRadius: 3,
  },
  itemFootInfo: {
    fontSize: 12,
    color: "#666666",
  },
  followButtonContainer: {
    height: 20, 
    width: 55, 
    backgroundColor: 'rgba(255, 255, 255, 0.3)', 
    borderRadius: 10, 
    justifyContent: "center",
  },
  followButtonText: {
    textAlign: "center", 
    textAlignVertical: "center", 
    color: "white", 
    fontWeight: 'bold',
  },
  footer: {
    flexDirection: 'row', 
    color: '#fff', 
    justifyContent: 'center', 
    alignItems: 'center', 
    height: 50,
  },
});

// Module name
AppRegistry.registerComponent("ZHColumn", () => ZHColumnModule);