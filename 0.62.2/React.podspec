Pod::Spec.new do |spec|

  spec.name         = "React"
  spec.version      = "0.62.2"
  spec.summary      = "It`s React frameworks."
  spec.description  = <<-DESC
                      It`s React frameworks. Core + Lib + Common.
                   DESC

  spec.homepage     = "https://gitlab.com/Malygos/react-native-lib"
  spec.license      = { :type => 'MIT', :file => '../LICENSE' }
  spec.author       = { "朱超鹏" => "z164757979@163.com" }
  spec.source       = { :git => "https://gitlab.com/Malygos/react-native-lib.git", :tag => "#{spec.version}" }
  spec.platform     = :ios, '9.0'
  spec.framework    = 'Foundation', 'UIKit'
  spec.static_framework = true
  
  spec.subspec 'Debug' do |subspec|
    subspec.vendored_frameworks = 'Framework/Debug/*.framework'
  end
  
  spec.subspec 'Release' do |subspec|
    subspec.vendored_frameworks = 'Framework/Release/*.framework'
  end
  
end
