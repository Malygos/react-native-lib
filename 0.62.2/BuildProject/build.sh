#!/bin/sh

set -e

# 环境
PROJ_CONFIGURATION="Release"

if [[ $1 == 'Debug' ]]; then
	PROJ_CONFIGURATION='Debug'
fi

# 目录相关
WORK_PATH=$(cd `dirname $0`; pwd)
BUILD_PATH="$WORK_PATH/build"
PRODUCTS_PATH="$WORK_PATH/../Framework/${PROJ_CONFIGURATION}"

# 打包相关
PROJ_WORKSPACE="BuildProject.xcworkspace"
PROJ_TARGET_SCHEME="BuildProject_Framework"

DEVICE_DIR="${PROJ_CONFIGURATION}-iphoneos"
SIMULATOR_DIR="${PROJ_CONFIGURATION}-iphonesimulator"
BUILD_OS_PATH="${BUILD_PATH}/${DEVICE_DIR}"
BUILD_SIMULATOR_PATH="${BUILD_PATH}/${SIMULATOR_DIR}"

echo $PRODUCTS_PATH
exit 1


# --------------------------------------------------
# 创建 Build/Product 目录
# --------------------------------------------------
# 创建 Build 目录
if [ -d "${BUILD_PATH}" ]
then
rm -rf "${BUILD_PATH}"
fi
mkdir -p "${BUILD_PATH}"

# 创建 Product 目录
if [ -d "${PRODUCTS_PATH}" ]
then
rm -rf "${PRODUCTS_PATH}"
fi
mkdir -p "${PRODUCTS_PATH}"

# --------------------------------------------------
# 编译真机和模拟器版本,务必要先编译模拟器，再编译真机版
# --------------------------------------------------
printf $(tput setaf 1; tput bold)'start build framework...\n'$(tput sgr0)
xcodebuild -configuration $PROJ_CONFIGURATION -workspace $PROJ_WORKSPACE -scheme $PROJ_TARGET_SCHEME SYMROOT=$BUILD_PATH -sdk iphonesimulator CLANG_ENABLE_CODE_COVERAGE=NO
xcodebuild -configuration $PROJ_CONFIGURATION -workspace $PROJ_WORKSPACE -scheme $PROJ_TARGET_SCHEME SYMROOT=$BUILD_PATH -sdk iphoneos CLANG_ENABLE_CODE_COVERAGE=NO

# --------------------------------------------------
# 删除无用文件
# --------------------------------------------------
exclude_files=('BuildProject_Framework.framework' 'BuildProject_Framework.framework.dSYM' 'Pods_BuildProject_Framework.framework')

for file in ${exclude_files[@]};
do
	fPath=${BUILD_OS_PATH}/${file}
	if [ -d "${fPath}" ]
	then
		rm -rf "${fPath}"
	fi

	fPath=${BUILD_SIMULATOR_PATH}/${file}
	if [ -d "${fPath}" ]
	then
		rm -rf "${fPath}"
	fi
done

# --------------------------------------------------
# 合并真机和模拟器framework
# --------------------------------------------------

function combineFramework() {
	if [ $# != 2 ] ; then 
		echo "打包失败"
		exit 1
	fi

	FW_OS_PATH="$1/$2";
	FW_SIMULATOR_PATH=${FW_OS_PATH/$DEVICE_DIR/$SIMULATOR_DIR}
	FW_NAME=${2%.framework}

	cp -R "${FW_OS_PATH}" "${PRODUCTS_PATH}"
	lipo -create "${FW_OS_PATH}/${FW_NAME}" "${FW_SIMULATOR_PATH}/${FW_NAME}" -output "${PRODUCTS_PATH}/${FW_NAME}.framework/${FW_NAME}"
}

os_path_list=`ls ${BUILD_OS_PATH}`
for path in $os_path_list;
do
	if [[ $path != *.framework && $path != *.bundle ]]; then
		fw_dir="${BUILD_OS_PATH}/${path}"
		if [[ -d $fw_dir ]]; then
			fw_list=`ls ${fw_dir}`
			for fw in $fw_list; do
				if [[ $fw == *.framework ]]; then
					combineFramework $fw_dir $fw
				fi
			done
		fi
	fi
done