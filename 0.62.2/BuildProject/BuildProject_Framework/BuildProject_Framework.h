//
//  BuildProject_Framework.h
//  BuildProject_Framework
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2021/1/14.
//  Copyright © 2021 朱超鹏(平安健康互联网健康研究院). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTRootView.h>

//! Project version number for BuildProject_Framework.
FOUNDATION_EXPORT double BuildProject_FrameworkVersionNumber;

//! Project version string for BuildProject_Framework.
FOUNDATION_EXPORT const unsigned char BuildProject_FrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BuildProject_Framework/PublicHeader.h>


